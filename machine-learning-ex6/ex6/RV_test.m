clc;clear;close all
l=[3 5];
x1=0:0.2:6;
x2=x1;
theta=2;
plot(x1, exp(-x1),'b');
hold on;
plot(x1, exp(-x1/theta),'g');
hold on;
theta=0.5;
plot(x1, exp(-x1/theta),'r');
legend('theta=1','theta=2','theta=0.5');